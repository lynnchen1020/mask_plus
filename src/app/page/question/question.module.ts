import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionRoutingModule } from './question-routing.module';
import { QuestionComponent } from './question.component';
import { ShareModule } from 'src/app/modules/share/share.module';



@NgModule({
  declarations: [QuestionComponent],
  imports: [
    CommonModule,
    ShareModule,
    QuestionRoutingModule
  ]
})
export class QuestionModule { }
