import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.sass']
})
export class QuestionComponent implements OnInit {
  currentIdx: number;

  constructor() {
    this.currentIdx = null;
  }

  ngOnInit(): void {
  }

  open(index: number) {
    if(index === this.currentIdx) {
      this.currentIdx = null;
      return;
    }

    this.currentIdx = index;
  }

}
