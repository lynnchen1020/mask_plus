import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Component, OnInit, AfterViewInit, Inject, PLATFORM_ID, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { moveFromWindow, getOffsetInWindow } from 'src/app/helper/tool';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('promoEl') promoEl: ElementRef;
  @ViewChild('vidSupport') vidSupport: ElementRef;
  @ViewChild('vidPitts') vidPitts: ElementRef;
  @ViewChild('vidPush') vidPush: ElementRef;
  // @ViewChild('specBreathEl') specBreathEl: ElementRef;
  // @ViewChild('video3El') video3El: ElementRef;
  // @ViewChild('imgModel') imgModel: ElementRef;
  currentBannerIdx = 0;
  currentItem: any;
  isOpen = false;
  elOffsetTop: any;
  isDone = false;
  countDown: any;
  timestamp: number;
  isSafari: boolean;
  isNavOpened = false;
  isSupportWebp = true;
  hasQuickBuy = false;

  aniConfig: any;
  gallery: string[];
  webPgallery: string[];
  currentGalleryImg: string;
  currentWebpGalleryImg: string;
  priceList = [
    {
      url: 'https://core.newebpay.com/EPG/mask_plus_1/ITleIg'
    },
    {
      url: 'https://core.newebpay.com/EPG/mask_plus_1/gVS3aN'
    },
  ]

  @HostListener('window:scroll', []) private onScroll():void {

    if (window.pageYOffset - (this.elOffsetTop.promoEl - 300) > 0) {
      // this.aniConfig.promoEl = true;
      this.hasQuickBuy = true;
    } else {
      this.hasQuickBuy = false;
    }

  //   if (window.pageYOffset - (this.elOffsetTop.specBreathEl) > -650 && window.pageYOffset - (this.elOffsetTop.specBreathEl) < 700) {
  //     this.aniConfig.specBreathEl = true;
  //   } else {
  //     this.aniConfig.specBreathEl = false;
  //   }

  //   // if (window.pageYOffset - (this.elOffsetTop.video3El - 200) > 0) {
  //   //   this.aniConfig.video3El = true;
  //   //   this.playVideo('video3El');
  //   // }

  };



  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: any,
    private titleService: Title,
    private metaService: Meta,
    private header: HeaderService) {
    this.aniConfig = {
      specSpaceEl: false,
      specBreathEl: false,
      // video3El: false
    }
    this.gallery = [
      'assets/img/air_mask/img_air_product_1.jpg',
      'assets/img/air_mask/img_air_product_2.jpg',
      'assets/img/air_mask/img_air_product_3.jpg',
      'assets/img/air_mask/img_air_product_4.jpg',
      'assets/img/air_mask/img_air_product_5.jpg',
      'assets/img/air_mask/img_air_product_6.jpg',
      'assets/img/air_mask/img_air_product_7.jpg',
      'assets/img/air_mask/img_air_product_8.jpg',
      'assets/img/air_mask/img_air_product_9.jpg',
    ];
    this.webPgallery = [
      'assets/img/webp/img_product_1.webp',
      'assets/img/webp/img_product_2.webp',
      'assets/img/webp/img_product_3.webp',
      'assets/img/webp/img_product_4.webp',
      'assets/img/webp/img_product_5.webp',
      'assets/img/webp/img_product_6.webp',
      'assets/img/webp/img_product_7.webp',
    ];
    this.currentGalleryImg = 'assets/img/air_mask/img_air_product_1.jpg';
    this.currentWebpGalleryImg = 'assets/img/webp/img_product_1.webp';
  }

  ngOnInit(): void {
    this.titleService.setTitle('【MIT 透氣立體口罩架】- MASK+ 好加在第二代口罩空氣架')
    this.metaService.addTags(
      [
        {
          name: 'description',
          content: 'MASK+好加在第二代口罩空氣架，不限臉型、鼻型，不貼臉，超輕巧，享受肌膚無感體驗！台灣製造專利設計，MASK+ 好加在口罩架系列解決長期以來戴口罩的不適感，適合每天需配戴口罩的你/妳，立體口罩架撐出大空間!'
        },
        {
          name: 'og:title',
          content: '【MIT 透氣立體口罩架】- MASK+ 好加在第二代口罩空氣架'
        },
        {
          name: 'og:description',
          content: 'MASK+好加在第二代口罩空氣架，不限臉型、鼻型，不貼臉，超輕巧，享受肌膚無感體驗！台灣製造專利設計，MASK+ 好加在口罩架系列解決長期以來戴口罩的不適感，適合每天需配戴口罩的你/妳，立體口罩架撐出大空間!'
        },
        {
          name: 'og:url',
          content: 'http://maskplus.com.tw'
        },
        {
          name: 'og:site_name',
          content: '【MIT 透氣立體口罩架】- MASK+ 好加在第二代口罩空氣架'
        },
        {
          name: 'og:image',
          content: 'http://maskplus.com.tw/assets/img/air_mask/img_mask_white.png'
        },
      ]
    )

    this.header.currentUrl = ''
    // const deadline = new Date("Mar 8, 2021 00:00:00").getTime();


    // interval(1000).subscribe(() => {
    //   this.countDownTimer(deadline);
    // });
  }

  ngAfterViewInit(): void {

    if(isPlatformBrowser(this.platformId)) {

      setTimeout(() => {
        this.elOffsetTop = {
          promoEl: this.promoEl.nativeElement.getBoundingClientRect().top

        };
      }, 500)
    }
    //   this.isSafari = /constructor/i.test(String(window.HTMLElement)) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof 'safari' !== 'undefined' && window['safari'].pushNotification));

      this.vidSupport.nativeElement.muted = true;
      this.vidPitts.nativeElement.muted = true;
      this.vidPush.nativeElement.muted = true;
      this.vidSupport.nativeElement.play();
      this.vidPitts.nativeElement.play();
      this.vidPush.nativeElement.play();
    // }
  }

  onImageLoad() {
    // if(isPlatformBrowser(this.platformId)) {
    //   this.isDone = true;
    // }
  }

  // public scrollToBottom() {
  //   if (isPlatformBrowser(this.platformId)) {
  //     const wrapperHeight = document.getElementById('WRAPPER').offsetHeight;
  //     window.scrollTo({
  //       left: 0,
  //       top: wrapperHeight,
  //       behavior: 'smooth'
  //     });
  //   }
  // }

  move(className: string){
    const el: HTMLElement = document.querySelector(`.${className}`) as HTMLElement
    if(el) {
      if(window.innerWidth <= 992) {
        moveFromWindow(true, getOffsetInWindow(el).top - 500 )
        return;
      }
      moveFromWindow(true, getOffsetInWindow(el).top)
    }
  }

  // public getCircleLen() {
  //   document.documentElement.style.setProperty('--circle-len', this.circle.nativeElement.getTotalLength());
  //   console.log('length', this.circle.nativeElement.getTotalLength())
  // }

  // public changeBanner(idx: number) {
  //   if (idx === 0) {
  //     this.currentBannerIdx = 1;
  //   } else {
  //     this.currentBannerIdx = 0;
  //   }
  // }


  checkout(url: string) {
    window.open(url, '_blank')
  }

  playVideo(video: string) {
    // console.log('playVideo')
    if (!this.aniConfig.video1El && !this.aniConfig.video2El && !this.aniConfig.video3El) {
      return;
    }

    const timeout = setTimeout(() => {
      this[video].nativeElement.muted = true;
      this[video].nativeElement.play().catch((e) => {
        console.log(e)
      });

      window.clearTimeout(timeout);
    }, 500)
  }

  changeImg(imgUrl: string) {
    this.currentGalleryImg = imgUrl;
  }

  changeWebpImg(imgUrl: string) {
    this.currentWebpGalleryImg = imgUrl;
  }

  toggleNav() {
    this.isNavOpened = !this.isNavOpened;
  }

  // countDownTimer(deadline: number) {
  //   const now = new Date().getTime();
  //   this.timestamp = deadline - now;
  //   const days = Math.floor(this.timestamp / (1000 * 60 * 60 * 24));
  //   const hours = Math.floor((this.timestamp % (1000 * 60 * 60 * 24))/(1000 * 60 * 60));
  //   const minutes = Math.floor((this.timestamp % (1000 * 60 * 60)) / (1000 * 60));
  //   const seconds = Math.floor((this.timestamp % (1000 * 60)) / 1000);

  //   this.countDown = {
  //     days,
  //     hours,
  //     minutes,
  //     seconds
  //   }
  // }

}
