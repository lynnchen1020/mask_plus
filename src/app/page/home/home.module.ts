import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GaClickDirective } from 'src/app/directive/ga-click.directive';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { ShareModule } from 'src/app/modules/share/share.module';


@NgModule({
  declarations: [HomeComponent, GaClickDirective],
  imports: [
    CommonModule,
    ShareModule,
    HomeRoutingModule
  ],
  exports: [
    GaClickDirective
  ]
})
export class HomeModule { }
