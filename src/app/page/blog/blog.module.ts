import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { ContentText0Component } from './content/content-text0/content-text0.component';
import { ContentComponent } from './content/content/content.component';
import { ContentText1Component } from './content/content-text1/content-text1.component';
import { ArticlesComponent } from './articles/articles.component';
import { ShareModule } from 'src/app/modules/share/share.module';
import { ContentText2Component } from './content/content-text2/content-text2.component';


@NgModule({
  declarations: [
    BlogComponent,
    ContentText0Component,
    ContentComponent,
    ContentText1Component,
    ContentText2Component,
    ArticlesComponent,
  ],
  imports: [
    CommonModule,
    ShareModule,
    BlogRoutingModule
  ]
})
export class BlogModule { }
