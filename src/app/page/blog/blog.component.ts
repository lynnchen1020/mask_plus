import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.sass']
})
export class BlogComponent implements OnInit {
  isNavOpened = false;
  blogList: any[];
  routerDate = '';

  constructor(
    public blog: BlogService,
    private titleService: Title,
    private router: Router
    ) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('MASK+ 好加在口罩框 - 選購指南')
  }

  get curTitle() {
    const result = this.blog.blogList.filter(list => {
      return list.routerDate === this.routerDate;
    });

    return result[0].title;
  }

  goTo(routerDate: string) {
    this.routerDate = routerDate;
    this.router.navigate([`blog/articles/${routerDate}/${this.curTitle}`])
  }

}
