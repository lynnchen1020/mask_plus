import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.sass']
})
export class ArticlesComponent implements OnInit {
  routerDate = '';

  constructor(
    private blog: BlogService,
    private activatedRoute: ActivatedRoute) {
  }

  get currentBlogRouterDate() {
    const result = this.blog.blogList.filter(list => {
      return list.routerDate === this.routerDate;
    });

    return result[0]?.routerDate;
  }

  ngOnInit(): void {
    this.routerDate = this.activatedRoute.snapshot.params.routerDate;
  }

}
