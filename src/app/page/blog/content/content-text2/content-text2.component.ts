import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ContentText0Component } from '../content-text0/content-text0.component';

@Component({
  selector: 'app-content-text2',
  templateUrl: './content-text2.component.html',
  styleUrls: ['./content-text2.component.sass']
})
export class ContentText2Component extends ContentText0Component implements OnInit {

  constructor(
    location: Location,
    titleService: Title,
    metaService: Meta
  ) {
    super(location, titleService, metaService)
  }

  ngOnInit(): void {
    this.titleService.setTitle('【MIT 透氣立體口罩架】- 小測驗！找出你適合哪一款 MASK+ 口罩空氣架？')
    this.metaService.addTags(
      [
        {
          name: 'description',
          content: '您可以透過下面的問題，來找出自己適合什麼樣的口罩空氣架喔！'
        },
        {
          name: 'og:title',
          content: '【MIT 透氣立體口罩架】- 小測驗！找出你適合哪一款 MASK+ 口罩空氣架？'
        },
        {
          name: 'og:description',
          content: '您可以透過下面的問題，來找出自己適合什麼樣的口罩空氣架喔！'
        },
        {
          name: 'og:url',
          content: 'http://maskplus.com.tw/blog/articles/20210515/不同臉型 MASK+ 口罩架配戴使用說明'
        },
        {
          name: 'og:site_name',
          content: '【MIT 透氣立體口罩架】- 小測驗！找出你適合哪一款 MASK+ 口罩空氣架？'
        },
        {
          name: 'og:image',
          content: 'http://maskplus.com.tw/assets/img/img_blog_content_1.jpg'
        },
      ]
    )
  }

}
