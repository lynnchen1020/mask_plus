import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentText2Component } from './content-text2.component';

describe('ContentText2Component', () => {
  let component: ContentText2Component;
  let fixture: ComponentFixture<ContentText2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentText2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentText2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
