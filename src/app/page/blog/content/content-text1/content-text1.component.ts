import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ContentText0Component } from '../content-text0/content-text0.component';

@Component({
  selector: 'app-content-text1',
  templateUrl: './content-text1.component.html',
  styleUrls: ['./content-text1.component.sass']
})
export class ContentText1Component extends ContentText0Component implements OnInit {

  constructor(
    location: Location,
    titleService: Title,
    metaService: Meta
  ) {
    super(location, titleService, metaService);
  }

  ngOnInit(): void {
    this.titleService.setTitle('【MIT 透氣立體口罩架】- 不同臉型 MASK+ 口罩架配戴使用說明')
    this.metaService.addTags(
      [
        {
          name: 'description',
          content: '我們客觀認為，口罩的防護效果在於保護口鼻的前擋布料，所以在這樣的架構來看，我們設計的MASK+好加在的概念，是將口罩空間往前釋放增加呼吸空間'
        },
        {
          name: 'og:title',
          content: '【MIT 透氣立體口罩架】- 不同臉型 MASK+ 口罩架配戴使用說明'
        },
        {
          name: 'og:description',
          content: '我們客觀認為，口罩的防護效果在於保護口鼻的前擋布料，所以在這樣的架構來看，我們設計的MASK+好加在的概念，是將口罩空間往前釋放增加呼吸空間'
        },
        {
          name: 'og:url',
          content: 'http://maskplus.com.tw/blog/articles/20210515/不同臉型 MASK+ 口罩架配戴使用說明'
        },
        {
          name: 'og:site_name',
          content: '【MIT 透氣立體口罩架】- 不同臉型 MASK+ 口罩架配戴使用說明'
        },
        {
          name: 'og:image',
          content: 'http://maskplus.com.tw/assets/img/img_blog_content_1.jpg'
        },
      ]
    )
  }

}
