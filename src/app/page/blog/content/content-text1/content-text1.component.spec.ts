import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentText1Component } from './content-text1.component';

describe('ContentText1Component', () => {
  let component: ContentText1Component;
  let fixture: ComponentFixture<ContentText1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentText1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentText1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
