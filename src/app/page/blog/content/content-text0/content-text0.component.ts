import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-content-text0',
  templateUrl: './content-text0.component.html',
  styleUrls: ['./content-text0.component.sass']
})
export class ContentText0Component implements OnInit {

  constructor(
    protected location: Location,
    protected titleService: Title,
    protected metaService: Meta) { }

  ngOnInit(): void {
    this.titleService.setTitle('【MIT 透氣立體口罩架】- MASK+ 選購指南-愛情、事業大突破! 2021口罩開運密笈')
    this.metaService.addTags(
      [
        {
          name: 'description',
          content: '2020大家都辛苦了喔~ (唐老師語氣)，我們台灣在防疫的成績有非常好的維持，大家的口罩的生活還滿意嗎?除了呼吸有夠悶以外，口罩能帶來什麼好運呢? 我們一起來看看'
        },
        {
          name: 'og:title',
          content: '【MIT 透氣立體口罩架】- MASK+ 選購指南-愛情、事業大突破! 2021口罩開運密笈'
        },
        {
          name: 'og:description',
          content: '2020大家都辛苦了喔~ (唐老師語氣)，我們台灣在防疫的成績有非常好的維持，大家的口罩的生活還滿意嗎?除了呼吸有夠悶以外，口罩能帶來什麼好運呢? 我們一起來看看'
        },
        {
          name: 'og:url',
          content: 'http://maskplus.com.tw/blog/articles/20210505/愛情、事業大突破!%202021口罩開運密笈'
        },
        {
          name: 'og:site_name',
          content: '【MIT 透氣立體口罩架】- MASK+ 選購指南-愛情、事業大突破! 2021口罩開運密笈'
        },
        {
          name: 'og:image',
          content: 'http://maskplus.com.tw/assets/img/img_blog_content_0.jpg'
        },
      ]
    )
  }

  goBack() {
    this.location.back();
  }

}
