import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentText0Component } from './content-text0.component';

describe('ContentText0Component', () => {
  let component: ContentText0Component;
  let fixture: ComponentFixture<ContentText0Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentText0Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentText0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
