import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaskplusComponent } from './maskplus.component';

describe('MaskplusComponent', () => {
  let component: MaskplusComponent;
  let fixture: ComponentFixture<MaskplusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaskplusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaskplusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
