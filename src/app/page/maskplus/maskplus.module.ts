import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaskplusRoutingModule } from './maskplus-routing.module';
import { ShareModule } from 'src/app/modules/share/share.module';
import { MaskplusComponent } from './maskplus.component';


@NgModule({
  declarations: [MaskplusComponent],
  imports: [
    CommonModule,
    ShareModule,
    MaskplusRoutingModule
  ]
})
export class MaskplusModule { }
