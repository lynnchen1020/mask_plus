import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaskplusComponent } from './maskplus.component';

const routes: Routes = [
  {
    path: '',
    component: MaskplusComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaskplusRoutingModule { }
