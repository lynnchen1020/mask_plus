import { Component } from '@angular/core';
import { GtmService } from './services/gtm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'mask-plus';

  constructor(private gtm: GtmService) {
    this.gtm.init();
  }
}
