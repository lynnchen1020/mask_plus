import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  isNavOpened = false;

  constructor(private router: Router, public header: HeaderService) {}
  @Input() place?: string;

  ngOnInit(): void {
  }

  toggleNav() {
    this.isNavOpened = !this.isNavOpened;
  }

  goToHomepage() {
    this.router.navigate(['/home']);
    this.header.currentUrl = '';
  }

  goTo(url: string) {
    this.router.navigate([`${url}`]);
    this.header.currentUrl = url;
  }

}
