import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appGaClick]'
})
export class GaClickDirective {

  @Input('appGaClick') option:any;

  @HostListener('click', ['$event']) onClick($event){

    (<any>window).ga('send', 'event', this.option.category, this.option.action, this.option.label, {
      hitCallback: function() {
      }

    });

  }
  constructor() { }

}
