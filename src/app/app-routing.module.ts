import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./page/home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'maskplus',
    loadChildren: () => import('./page/maskplus/maskplus.module').then(m => m.MaskplusModule),
  },
  {
    path: 'story',
    loadChildren: () => import('./page/story/story.module').then(m => m.StoryModule)
  },
  {
    path: 'question',
    loadChildren: () => import('./page/question/question.module').then(m => m.QuestionModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./page/blog/blog.module').then(m => m.BlogModule),
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'top'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
