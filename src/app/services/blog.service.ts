import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  blogList = [
    {
      title: '小測驗！找出你適合哪一款 MASK+ 口罩空氣架？',
      description: '透過下面的簡單問題，來找出自己適合什麼樣的口罩空氣架喔!',
      date: new Date('2021-12-20').getTime(),
      routerDate: '20211220',
      coverImg: 'assets/img/img_blog_content_2.jpg'
    },
    {
      title: '不同臉型 MASK+ 口罩架配戴使用說明',
      description: '我們客觀認為，口罩的防護效果在於保護口鼻的前擋布料，所以在這樣的架構來看，我們設計的MASK+好加在的概念，是將口罩空間往前釋放增加呼吸空間',
      date: new Date('2021-05-15').getTime(),
      routerDate: '20210515',
      coverImg: 'assets/img/img_blog_content_1.png'
    },
    {
      title: '愛情、事業大突破! 2021口罩開運密笈',
      description: '2020大家都辛苦了喔~ (唐老師語氣)，我們台灣在防疫的成績有非常好的維持，大家的口罩的生活還滿意嗎? 除了呼吸有夠悶以外',
      date: new Date('2021-05-05').getTime(),
      routerDate: '20210505',
      coverImg: 'assets/img/img_blog_content_0.jpg'
    },
  ]

  constructor() { }
}
