import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Inject, PLATFORM_ID } from '@angular/core';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

declare global {
  interface Window {
    dataLayer: any[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class GtmService {
  window: Window;
  gtmCode: string;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private readonly platformId: Object,
  ) {
    if (isPlatformBrowser(this.platformId)) {

      this.window = this.document.defaultView;
      if (!this.window.dataLayer) {
        this.window.dataLayer = [];
      }
    }


    this.gtmCode = 'GTM-NHBGC6S';
    // this.gtmCode = environment.production ? '' : 'GTM-NHBGC6S';
  }

  public init() {
    if(!environment.production) {
      return;
    }
    this.mountGtmScript();
    this.mountFBpixel();
  }

  private mountFBpixel() {
    if (isPlatformBrowser(this.platformId)) {
      const fbScript = document.createElement('script');
      const fbNoScript = document.createElement('noscript');

      fbScript.type = 'text/javascript';

      fbScript.innerHTML = `!function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '828292764439531');
      fbq('track', 'PageView');`

      fbNoScript.innerHTML = `<img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=828292764439531&ev=PageView&noscript=1"
      />`

      this.document.head.appendChild(fbScript);
      this.document.head.appendChild(fbNoScript);
    }
  }

  private mountGtmScript() {
    if (isPlatformBrowser(this.platformId)) {
      const gtmFirstScript = document.createElement('script');
      const gtmSecondScript = document.createElement('noscript');

      gtmFirstScript.type = 'text/javascript';

      gtmFirstScript.innerHTML = `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','${this.gtmCode}');`

      gtmSecondScript.innerHTML = `<iframe src="https://www.googletagmanager.com/ns.html?id=${this.gtmCode}" height="0" width="0" style="display:none;visibility:hidden"></iframe>`;

      this.document.head.appendChild(gtmFirstScript);
      this.document.body.appendChild(gtmSecondScript);
    }
  }
}
