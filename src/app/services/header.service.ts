import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  currentUrl: string;

  constructor() {
    this.currentUrl = ''
  }
}
